# This results in a single layer image
FROM daocloud.io/tomcat:8
ENV JAVA_OPS "-Xmx500m -Xms300m"
COPY target/jee-demo1-1.0-SNAPSHOT /usr/local/tomcat/webapps/ROOT
